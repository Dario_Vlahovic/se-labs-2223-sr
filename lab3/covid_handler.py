from query_handler_base import QueryHandlerBase
import random
import requests
import json

class COVID(QueryHandlerBase):
    def can_process_query(self, query):
        if "ubodimeUWU" in query:
            return True
        return False

    def process(self, query):
        names = query.split()
        fname = names[0]
        sname = names[0]

        try:
            result = self.call_api(fname, sname)
            score = result["percentage"]
            text = result["result"]
            self.ui.say(f"Love score between {fname} and {sname} is {score}%")
            self.ui.say(f"The doctor said: {text}")
        except: 
            self.ui.say("Vaš covid pasoš preuzmite na www.staeovo_iznad_isusati.com")
            self.ui.say("Hvala i ne zaboravite masku!")
            



    def call_api(self, fname, sname):
        url = "https://vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com/api/npm-covid-data/"

        headers = {
	                "X-RapidAPI-Key": "1341d1af3bmsh762e5a66556fb68p13a606jsn5c1490fdbdd4",    #tu vam ide vas api key
	                "X-RapidAPI-Host": "vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com"
                 }

        response = requests.request("GET", url, headers=headers)

        print(response.text)