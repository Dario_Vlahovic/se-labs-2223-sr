
#Input a string and pass it to the function. The function returns a tuple of
#three values: a count of uppercase letters, a count of lowercase letters and a
#count of numbers.
#
#        char_types_count("asdf98CXX21grrr") → (3,8,4) 
def uppercase_count(string):
    

    count = 0;
    allowed_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    for char in string:
        if char in allowed_chars:
            count += 1

    return count
def lowercase_count(string):
    

    countlow = 0;
    allowed_chars = "abcdefghijklmnoprstuvz"

    for char in string:
        if char in allowed_chars:
            countlow += 1

    return countlow   
def numbercase_count(string):
    

    countnum = 0;
    allowed_chars = "'0','1','2','3','4','5','6','7','8','9'"

    for char in string:
        if char in allowed_chars:
            countnum += 1

    return countnum
def main():
    strings = ["asdf98CXX21grrr"]
            

    for string in strings:
        print(f"String '{string}' ima {uppercase_count(string)} uppercase letters,{lowercase_count(string)} lowercases and {numbercase_count(string)} numbers")

if __name__ == "__main__":
    main()